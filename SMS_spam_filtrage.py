import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import string
string.punctuation
from nltk.stem import PorterStemmer
import nltk
from nltk.corpus import stopwords
from sklearn.feature_extraction.stop_words import ENGLISH_STOP_WORDS
import sklearn
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import accuracy_score, confusion_matrix, precision_score, recall_score, roc_auc_score
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score
from sklearn.svm import SVC
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import accuracy_score,classification_report,confusion_matrix
from sklearn.model_selection import cross_val_score
import seaborn as sns

#lecteur de fichier et representation sous forme matricielle
df_SMS = open('D:/L3/Projet-Tuto/SMSSpamCollection.txt').read()
df_SMS= df_SMS.replace('\n','\t')
list_SMS = df_SMS.split("\t")
labels = list_SMS[0::2]
labels = labels[:len(labels)-1]
text   = list_SMS[1::2]
df_SMS= pd.DataFrame(list(zip(text, labels)))
df_SMS.columns = ['text', 'labels']
print(df_SMS.shape)
print(df_SMS.labels.unique())
print(df_SMS.head(5))



#PRETRAITEMEMENT

#retirer les punctuation
def remove_punkt(string_):
    word_list_no_punkt = "".join([char for char in string_ if char not in string.punctuation])
    return word_list_no_punkt
df_SMS['cleaned_text'] = df_SMS['text'].apply(lambda x: remove_punkt(x))
df_SMS['cleaned_text'] = df_SMS['cleaned_text'].apply(lambda x: str.lower(x))
df_SMS= df_SMS[['labels', 'text', 'cleaned_text']]

#tokenization
from nltk.tokenize import word_tokenize
df_SMS['tokenized_words'] = df_SMS['cleaned_text'].apply(lambda x: word_tokenize(x))

#stopwords ENGLISH
stopwords_nltk = list(stopwords.words('english'))

from sklearn.feature_extraction import text
stopwords_sklearn = list(text.ENGLISH_STOP_WORDS)
stopwords_merged= list(set(stopwords_nltk + stopwords_sklearn ))
stopwords_merged = sorted(stopwords_merged)

def remove_stopwords(tokenized_words):
    stopwords_removed = [word for word in tokenized_words if word not in stopwords_merged]
    return stopwords_removed
df_SMS['stop_words_removed'] = df_SMS['tokenized_words'].apply(lambda x: remove_stopwords(x))

#lemmitization
wnlm = nltk.WordNetLemmatizer()
def lemmatize(tokens):
    lemmatized = [wnlm.lemmatize(token) for token in tokens]
    return lemmatized
df_SMS['lemmatized_words'] = df_SMS['stop_words_removed'].apply(lambda x: lemmatize(x))
#stemming
ps = PorterStemmer()
def stemSet(wordList):
    wordSet = []
    for word in wordList:
        wordSet.append(ps.stem(word))
    return wordSet
df_SMS['stemWordSet'] = df_SMS['lemmatized_words'].apply(lambda x:stemSet(x))

#nombre de carachter
df_SMS['length_text'] = df_SMS['text'].apply(lambda x: len(x) - x.count(" "))
#nombre de lettre en maj
def capital_letters(text):
    k = sum([1 for i in text if i.isupper()])
    capital_percent = k/(len(text) - text.count(" "))*100
    return round(capital_percent,3)
df_SMS['capital%'] = df_SMS['text'].apply(lambda x: capital_letters(x))

#procentage de punctuation
def punc_percent(k):
    punkt = sum([1 for i in k if i in string.punctuation])
    return round(punkt/(len(k) - k.count(" "))*100,3)
df_SMS['punc%'] = df_SMS['text'].apply(lambda x: punc_percent(x))

#le text final
print(df_SMS.head())
df_SMS['final_text']=[" ".join(text) for text in df_SMS['lemmatized_words'].values]


#Tf_idf
tfidf_vec = TfidfVectorizer()
X_tfidf = tfidf_vec.fit_transform(df_SMS['final_text'])


y = df_SMS['labels']
from sklearn import preprocessing
lb = preprocessing.LabelBinarizer()
y = lb.fit_transform(y)

#split dataframe on train et test
X_train, X_test, y_train, y_test = train_test_split(X_tfidf, y, test_size=0.3)

#Knn algorithme
X_train.shape
model=KNeighborsClassifier(n_neighbors=3)
model.fit(X_train,np.ravel(y_train,order='C'))
y_pred=model.predict(X_test)

#mesure d'evaluation de KNN
print (accuracy_score(y_test, y_pred))
print (classification_report(y_test, y_pred))
print (confusion_matrix(y_test,y_pred))


#NB algorithme
mnb = MultinomialNB(alpha=0.35)
mnb.fit(X_train, np.ravel(y_train,order='C'))
#testing naive bayes
prediction_NB = mnb.predict(X_test)
print(accuracy_score(y_test,prediction_NB))

#svm algorithme
svc = SVC(kernel='sigmoid', gamma=1.0)
svc.fit(X_train, np.ravel(y_train,order='C'))
prediction_SVM = svc.predict(X_test)
print(accuracy_score(y_test,prediction_SVM))

total_len = len(df_SMS['labels'])
percentage_labels = (df_SMS['labels'].value_counts()/total_len)*100
percentage_labels
sns.set()
sns.set_palette("hls", 8)

sns.countplot(df_SMS['labels']).set_title('Data Distribution for target variable')
ax = plt.gca()
for p in ax.patches:
    height = p.get_height()
    ax.text(p.get_x() + p.get_width()/2.,
            height + 2,
            '{:.2f}%'.format(100*(height/total_len)),
            fontsize=14, ha='center', va='bottom')
sns.set(font_scale=3)
ax.set_xlabel("Class distribution")
ax.set_ylabel("Numbers of records")
plt.show()


